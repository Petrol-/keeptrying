using UnityEngine;
using System.Collections;


public class JumpPlayer : MonoBehaviour {

	public float jumpVelocity;
	public CNJoystick jumpButton;
	private LayerMask LayerGround;
	private bool isGrounded;
	private Transform groundCheckL,groundCheckR;
	private RaycastHit2D[] hits;



	void Awake()
	{
		groundCheckL = transform.Find("GroundCheckL");
		groundCheckR = transform.Find("GroundCheckR");
		LayerGround = LayerMask.NameToLayer ("Ground");
		hits = new RaycastHit2D[1];
	}
    void Start()
    {
        jumpButton.FingerTouchedEvent += HandlerFingerTouched;
    }
	// Update is called once per frame
	void Update () {

		isGrounded = ((Physics2D.LinecastNonAlloc (transform.position, groundCheckL.position, hits, (1 << LayerGround))) 
		              |
		             (Physics2D.LinecastNonAlloc (transform.position, groundCheckR.position, hits, (1 << LayerGround))))
					== 1 ? true : false;

        //UnityAnswer Test
        
        if (Input.touchCount > 0)
        {
            Debug.Log("Unity Answer Test");

            Touch touch = Input.touches[0];

          //  if (touch.phase == TouchPhase.Began)
            if(true)
            {

                Application.LoadLevel("Menu");

            }
        }
	}

	private void Jump(){

		if (isGrounded) 
		{
			rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x,jumpVelocity);
			isGrounded = false;
		}
	}
	private void HandlerFingerTouched(){
		Debug.Log ("HandlerFingerTouched... Jumping");
		Jump ();
		}
}
