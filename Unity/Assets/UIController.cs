using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins;

public delegate void PanelHasMovedEventHandler();
public class UIController : MonoBehaviour {

    //PausePanel
    public event PanelHasMovedEventHandler PausePanelHidden;
    public GameObject pausePanel;
    public Vector3 pauseTarget;
    public float pauseTransitionDuration;
    public Ease easeType;
    private Vector3 pauseOrigin;
    private bool pausePanelInPosition = false;
    private Tweener tPause;
    
    //\\//

    private static UIController _instance;
    public static UIController Instance
    {
        //Singleton initialization
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<UIController>(); 
            return _instance;
        }
        //\\//

        //DOTween Initialization
        
       
    }
    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            if (_instance != this)
                Destroy(this);
        }
        DOTween.Init(false, true, LogBehaviour.Default);
        
    }

    void Start()
    {
        pauseOrigin = pausePanel.transform.localPosition;
        Debug.Log("PausePanel localPosition " + pausePanel.transform.localPosition.ToString());
        
       
       }

    public void TooglePausePanel()
    {
       
       if(pausePanelInPosition){
           pausePanel.transform.DOLocalMove(pauseOrigin, pauseTransitionDuration).OnComplete(PanelIsHidden).SetEase(easeType);
           pausePanelInPosition = !pausePanelInPosition;
           Debug.Log("PausePanel Hiding");
        }
        
        else
        {
            pausePanel.transform.DOLocalMove(pauseTarget, pauseTransitionDuration).OnComplete(PanelIsShowing    ).SetEase(easeType);
            pausePanelInPosition = !pausePanelInPosition;
            Debug.Log("PausePanel showing");
        }
    
    }

    private void PanelIsHidden()
    {
       // Debug.Log("PausePanel new location " + pausePanel.transform.localPosition.ToString());
       
        PausePanelHidden();
        
    }
    private void PanelIsShowing()
    {

    }
}
