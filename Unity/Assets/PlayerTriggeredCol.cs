﻿using UnityEngine;
using System.Collections;

public class PlayerTriggeredCol : MonoBehaviour {

    public Interaction triggerType;
    

    void OnTriggerEnter2D(Collider2D obj)
    {
        if (GameController.Instance.debug)
            return;


        switch (triggerType)
        {
            case Interaction.DEATH:
                GameController.Instance.PlayerDied();
                break;
            case Interaction.WIN:
                GameController.Instance.PlayerWon();
                break;
        }
    }
}
