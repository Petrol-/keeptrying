﻿using UnityEngine;
using System.Collections;

public class PlayerMoveX : MonoBehaviour {

	public CNJoystick joystick;
	public float movementSpeed;
	public bool normalizeSpeed;
	private Vector2 movement;
    private bool _canMove;
    public bool canMove
    {
        get
        {
            return _canMove;
        }
        set
        {
            _canMove = value;
        }
    }
	void Awake()
	{
		//Register To the event
		joystick.JoystickMovedEvent += HandlerJoystickMoved;
		joystick.FingerLiftedEvent += HandlerFingerLifted;
		}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (canMove)
        {
            movement = new Vector2(movement.x, rigidbody2D.velocity.y);
            rigidbody2D.velocity = movement;
        }

	}

	private void HandlerJoystickMoved(Vector3 relativeVector){

		if (normalizeSpeed) {
			Vector2 vectorNormalized = relativeVector;
			vectorNormalized.Normalize();
			movement = new Vector2 (vectorNormalized.x * movementSpeed, rigidbody2D.velocity.y);
			return;
		}

		movement = new Vector2 (relativeVector.x * movementSpeed, rigidbody2D.velocity.y);
	}

	private void HandlerFingerLifted(){
		movement = new Vector2 (0.0f, rigidbody2D.velocity.y);
		}

	
}
