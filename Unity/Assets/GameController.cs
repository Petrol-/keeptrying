using UnityEngine;
using System.Collections;

public enum GameState
{
    STARTING,
    STARTED,
    PAUSED,
    ENDING,
    ENDED
}

public delegate void GameStateChangedEventHandler(GameState State);
public class GameController : MonoBehaviour {

   public event GameStateChangedEventHandler ActualGameState;
   public bool debug;
   public bool showDebugSprites;
   public float timeBeforeResume;

   private static GameController _instance;
   private KCtrlRoot KCtrl;
   private GameState gameState;
   private GameObject pausePanel;
   private CNJoystick joystick;

    
   public static GameController Instance
   {
      get
      {
          if (_instance == null)
          {
              _instance = FindObjectOfType<GameController>();
              
          }
          return _instance;
      }
  }
   void Awake()
   {
       //Initialization of the Singleton
       if (_instance == null)
       {
           _instance = this;
       }
       else
       {
           if (this != _instance)
               Destroy(this.gameObject);
       }

        //\\//
       //Set Fullscreen (hide the control bar for android devices prio to 4.4)
       Screen.fullScreen = true;
       UIController.Instance.PausePanelHidden += PausePanelIsHidden;

#region References
       //Find KCtrlc so this Object can receive Button's calls (e.g Pause Button)
       KCtrl = FindObjectOfType<KCtrlRoot>();
       pausePanel = GameObject.FindGameObjectWithTag("PausePanel");
#endregion

#region EventRegistration
       //Register to ActualGameStateEvent
       ActualGameState += GameStateChangedHandler;
#endregion
   }
    void Start()
    {
        //Set the Game in Starting Mode
        //DISABLED FOR DEBUGGING
       // ActualGameState(GameState.STARTING);
        //\\//
        ActualGameState(GameState.STARTED);

        //get Pause button status
        KCtrl.AddMessage("button", "begin", "Pause", "PauseButtonTouched", this.gameObject);
        joystick = FindObjectOfType<CNJoystick>();
        
    }

    
	// Update is called once per frame
	void Update () {
	
		if (Input.GetKey (KeyCode.Escape))
						Application.LoadLevel (Application.loadedLevel);
	}

    public void PauseButtonTouched()
    {
        if (gameState == GameState.PAUSED)
        {
            UIController.Instance.TooglePausePanel();
        }
        else
        {
            ActualGameState(GameState.PAUSED);
            Debug.Log("Game Paused");
            KCtrl.SetDisplayEnable(false);
            KCtrl.SetInputEnable(false);
            UIController.Instance.TooglePausePanel();
        }
    }

    public void RestartButtonTouched()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
    private void GameStateChangedHandler(GameState state)
    {
        switch (state)
        {
            case GameState.STARTING: gameState = GameState.STARTING; break;
            case GameState.STARTED: gameState = GameState.STARTED; break;
            case GameState.PAUSED: gameState = GameState.PAUSED; break;
            case GameState.ENDING: gameState = GameState.ENDING; break;
            case GameState.ENDED: gameState = GameState.ENDED; break;
        }
    }

    public void PlayerDied()
    {
        Debug.Log("Player Died");
        ActualGameState(GameState.ENDING);
    }

    public void PlayerWon()
    {
        Debug.Log("Player Won");
        ActualGameState(GameState.ENDING);
    }


    private void  PausePanelIsHidden()
    {
        Debug.Log("Pause Panel is Hidden");
        StartCoroutine("ResumeGameAfterSeconds", timeBeforeResume);

        
    }

    private IEnumerator ResumeGameAfterSeconds(float time)
    {
        KCtrl.SetDisplayEnable(true);
        KCtrl.SetInputEnable(true);
        yield return new WaitForSeconds(timeBeforeResume);
        ActualGameState(GameState.STARTED);
        Debug.Log("Game Resumed");

        yield return null;

    }
}
