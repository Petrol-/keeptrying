using UnityEngine;
using System.Collections;

public class JumpPlayerWithKctrl : MonoBehaviour {

    public float jumpVelocity;
    private bool _canJump;
    private LayerMask LayerGround;
    private bool isGrounded;
    private Transform groundCheckL, groundCheckR;
    private RaycastHit2D[] hits;
    private KCtrlRoot KCtrl;

    public bool canJump
    {
        get { return _canJump; }
        set { _canJump = value; }
    }

    void Awake()
    {
        
        groundCheckL = transform.Find("GroundCheckL");
        groundCheckR = transform.Find("GroundCheckR");
        LayerGround = LayerMask.NameToLayer("Ground");
        hits = new RaycastHit2D[1];
        KCtrl = FindObjectOfType<KCtrlRoot>();

    }

    void Start()
    {
        bool addreceived;
        addreceived = KCtrl.AddMessage("button", "begin", "Jump", "OnButtonTouch", this.gameObject);
        Debug.Log("Add Jump action :" + addreceived);
      
    }
    // Update is called once per frame
    void Update()
    {

        isGrounded = ((Physics2D.LinecastNonAlloc(transform.position, groundCheckL.position, hits, (1 << LayerGround)))
                      |
                     (Physics2D.LinecastNonAlloc(transform.position, groundCheckR.position, hits, (1 << LayerGround))))
                    == 1 ? true : false;
    }

    private void Jump()
    {

        if (isGrounded && _canJump)
        {
            rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, jumpVelocity);
            isGrounded = false;
        }
    }

    public void OnButtonTouch(ButtonDataOutput data)
    {
        Jump();
        Debug.Log("OnButtonTouch : Jump");
    }
}
