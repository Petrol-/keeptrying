﻿using UnityEngine;
using System.Collections;

public class EffectsController : MonoBehaviour {
    //Singleton
    private static EffectsController _instance;
    private GameObject player;
    public static EffectsController  Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<EffectsController>();
            return _instance;
        }
    }

    public GameObject[] deathEffets; //Prefabs of deaths effects (Skulls)
    public GameObject[] winEffetcs; //Prefabs of Win effects


    void Awake()
    {
        if (_instance == null)
            _instance = this;

        player = GameObject.FindGameObjectWithTag("Player");
    }

    //Plays a random effect from deathEffects Array at Position Transform position.position
    public void PlayDeathCFXEffect(Vector3 position)
    {
        PlayRandomCFXEffect(deathEffets, position);  
    }

    //Plays a random effect from winEffects Array at Position Transform position.position
    public void PlayWinCFXEffect(Vector3 position)
    {
        GameObject obj = GetNextCFXObject(winEffetcs);
        if (obj != null)
        {
            obj.transform.parent = player.transform;
            obj.transform.localPosition = Vector3.zero;
            obj.transform.localRotation = Quaternion.identity;
        }
    }
    public void PlayCFXEffect(GameObject effect, Vector3 position)
    {
        GameObject obj = CFX_SpawnSystem.GetNextObject(effect);
        obj.transform.position = position;
    }

    //Plays a random Effect fron an array at Position Transform position.position
    private void PlayRandomCFXEffect(GameObject[] effects, Vector3 position)
    {
        //If the objects are loaded and the Array populated, play an effect
        if (CFX_SpawnSystem.AllObjectsLoaded && effects.Length>0)
        {
            GameObject effect = CFX_SpawnSystem.GetNextObject(effects[Random.Range(0, effects.Length)]);
            effect.transform.position = position;
            Debug.Log("Playing effect: " + effect.name);
        }
        else
        {
            Debug.Log("Effects are still loading or Array contains no effect");
        }
    }

    private GameObject GetNextCFXObject(GameObject[] effects)
    {
        return CFX_SpawnSystem.GetNextObject(effects[Random.Range(0, effects.Length)]);
    }

}
