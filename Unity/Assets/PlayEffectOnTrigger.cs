﻿using UnityEngine;
using System.Collections;

public enum Interaction
{
    WIN,
    DEATH
}

public class PlayEffectOnTrigger : MonoBehaviour {

    public Interaction effect;
    

    void Awake()
    {

        GetComponent<SpriteRenderer>().enabled = GameController.Instance.showDebugSprites;
            
    }

    void OnTriggerEnter2D(Collider2D obj)
    {
        switch (effect)
        {
            case Interaction.DEATH: 
                EffectsController.Instance.PlayDeathCFXEffect(transform.position);
                break;
            case Interaction.WIN: 
                EffectsController.Instance.PlayWinCFXEffect(transform.position);
                break;
        }
    }
}
