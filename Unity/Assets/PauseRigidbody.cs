﻿using UnityEngine;
using System.Collections;

public class PauseRigidbody : MonoBehaviour {

    private Vector2 currentVelocity;
    void Awake()
    {
        //Register to the event
        GameController.Instance.ActualGameState += new GameStateChangedEventHandler(GameStateChangedHandler);
        currentVelocity = new Vector2();
    }

   private void ResumeRigidBody()
    {
        rigidbody2D.isKinematic = false;
        rigidbody2D.velocity = currentVelocity;
       // Debug.Log("ResumeRigidBody, Velocity " + currentVelocity.y);
    }
   private void PauseRigidBody()
   {
       this.currentVelocity = new Vector2(rigidbody2D.velocity.x, rigidbody2D.velocity.y);
       //Debug.Log("PauseRigidBody, Velocity " + rigidbody2D.velocity.y);
       rigidbody2D.isKinematic = true;
       
   }
    private void GameStateChangedHandler(GameState state)
    {
        switch (state)
        {
            case GameState.STARTING :
            case GameState.PAUSED: PauseRigidBody(); break;
            case GameState.STARTED: ResumeRigidBody(); break;
            
        }
    }
}
