using UnityEngine;
using System.Collections;
using DG.Tweening;
using DG.Tweening.Core;

public class MenuUIController : MonoBehaviour {


    private enum Panel
    {
        MAINMENU,
        LEVELSELECTION,
        OPTION
    }

    private static MenuUIController _instance;
    public static MenuUIController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<MenuUIController>();
            }
            return _instance;
        }
    }

    public GameObject mainMenu;
    public GameObject levelselectionMenu;
    public GameObject option;
    public Ease easeType;
    public float transitionTime;
    public Vector3 mainMenuDestination;

    private Panel visiblePanel;
    private Vector3 optionPanelOrigin, levelSelectPanelOrigin;

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(this);
        }
        DOTween.Init(false, false);
        optionPanelOrigin = option.transform.localPosition;
        levelSelectPanelOrigin = levelselectionMenu.transform.localPosition;
        visiblePanel = Panel.MAINMENU;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ReturnButtonTouched();
        }
    }
    public void PlayButtonTouched()
    {
        mainMenu.transform.DOLocalMove(mainMenuDestination, transitionTime).SetEase(easeType);
        levelselectionMenu.transform.DOLocalMove(Vector3.zero,transitionTime).SetEase(easeType);
        visiblePanel = Panel.LEVELSELECTION;
    }
    public void OptionButtonTouched()
    {
        mainMenu.transform.DOLocalMove(mainMenuDestination, transitionTime).SetEase(easeType);
        option.transform.DOLocalMove(Vector3.zero, transitionTime).SetEase(easeType);
        visiblePanel = Panel.OPTION;
    }

    public void ReturnButtonTouched()
    {
        switch (visiblePanel)
        {
            case Panel.MAINMENU: Quit(); break;
            case Panel.LEVELSELECTION :
                levelselectionMenu.transform.DOLocalMove(levelSelectPanelOrigin, transitionTime).SetEase(easeType);
                mainMenu.transform.DOLocalMove(Vector3.zero, transitionTime).SetEase(easeType);
                visiblePanel = Panel.MAINMENU;
                break;
            case Panel.OPTION :
                option.transform.DOLocalMove(optionPanelOrigin, transitionTime).SetEase(easeType);
                mainMenu.transform.DOLocalMove(Vector3.zero, transitionTime).SetEase(easeType);
                visiblePanel = Panel.MAINMENU;
                break;
        }
    }

    private void Quit()
    {
        Application.Quit();
    }
}
