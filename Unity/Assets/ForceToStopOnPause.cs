﻿using UnityEngine;
using System.Collections;

public class ForceToStopOnPause : MonoBehaviour {

    private PlayerMoveX moveX;
    private JumpPlayerWithKctrl jump;
    void Awake()
    {
        GameController.Instance.ActualGameState += GameStateChanged;
        moveX = GetComponent<PlayerMoveX>();
        jump = GetComponent<JumpPlayerWithKctrl>();

    }

    private void EnablePlayerMovement()
    {
        moveX.canMove = true;
        jump.canJump = true;
        
    }
    private void DisablePlayerMovement()
    {
        moveX.canMove = false;
        jump.canJump = false;
    }
    private void GameStateChanged(GameState state)
    {
        switch (state)
        {
            case GameState.STARTED: EnablePlayerMovement(); break;
            default: DisablePlayerMovement(); break;
        }
    } 
}
